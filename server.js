//version inicial

let express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

let path = require('path');
let cors = require('cors');

app.use(cors());

app.use(express.static(__dirname+'/build/default'));

app.listen(port);
console.log(`http://localhost:${port}`);
console.log('todo list RESTful API server started on: ' + port);

/*############################
//#modulos con el Metodo GET #
//############################*/
app.get(`/`,getIndex);

function getIndex(req, res){
  res.sendFile('index.html',{root:'.'});
}

function getSingleClient(req, res){
  res.send(`Aqui tiene al cliente ${req.params.idcliente}`);
}

function getMovimientos(req, res){
  res.sendfile(`movimientosv1.json`);
}

function getMovimientosV2(req, res){
  res.send(movimientosJSON);
}

function getMovimientosV2SingleMove(req, res){
  console.log(`${req.params.idmove}`)
  res.send(movimientosJSON[req.params.idmove - 1]);
}

function getMovimientosV2Query(req, res){
  console.log(`${req.query.q}`); //ejemplo de URL http://localhost:3000/V02/Movimientosq?q=hola+mundo+node
  res.send(`Query recibidos`);
}

//funciones con Metodo POST 
function createPost(req,res){
  res.send(`Hola este es el post Cambiada`);
}

function moveCreateUser(req, res){
  let newRegister = req.body;

  newRegister.id = movimientosJSON.length + 1;
  movimientosJSON.push(newRegister);
  res.send("movimiento dado de alta");

}

//funciones con metodo PUT
function editUser(req,res){
  res.send(`Hola este es el put`);
}

//funciones con metodo DELETE

function deleteUser(req,res){
  res.send(`Hola este es el delete`);
}
